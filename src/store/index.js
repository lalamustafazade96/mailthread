import {createStore} from "redux";

const initialState = [{key: 1, name: 'test', type: 'test1', date: '2023-04-05', status: 0,number: 1}];


const dataReducer = (state = initialState, action) => {
    if (action.type === 'send') {
        // eslint-disable-next-line no-unused-expressions

        let newData = action.newData;
        const old = state.find(st => st.key == newData.key);
        if (old) {
            old.name = newData.name;
            old.type = newData.type;
            old.template = newData.template;
            old.from = newData.from;
            old.to = newData.to;
            old.customerName = newData.customerName;
            old.date = newData.date;
            old.subject = newData.subject;
            old.contentName = newData.contentName;
            old.feedLink = newData.feedLink;
            old.contentTemplate = newData.contentTemplate;
            old.contentText = newData.contentText;
            old.status = newData.status;
        } else
            return [newData, ...state]

    }
    return state
}
const store = createStore(dataReducer);
export default store;