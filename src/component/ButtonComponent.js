import React from "react";
import '../assets/scss/mailThreadCss.scss';
import {Button} from "react-bootstrap";

const ButtonComponent = (props) => {
    return (
        <Button className='send-btn' onClick={props.btnClick}>{props.name}</Button>)
}
export default ButtonComponent;