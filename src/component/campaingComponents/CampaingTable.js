import React from "react";
import {Table} from "react-bootstrap";

const CampaingTable = (props) => {
    return (
        <Table responsive >
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {props.tdata}
            </tbody>
        </Table>
    )
}
export default CampaingTable;