import React, {useState} from "react";
import {Button, Container, Form} from "react-bootstrap";
import "../../assets/scss/mailThreadCss.scss"
import ButtonComponent from "../ButtonComponent";
import CampaingTable from "./CampaingTable";
import {Link, useHistory} from "react-router-dom";
import {useSelector} from "react-redux";
import Modal from 'react-bootstrap/Modal';
import '../../assets/scss/mailThreadCss.scss'

const Campaing = () => {

    const storeData = useSelector(state => state);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showDataModal, setShowDataModal] = useState(false);
    const [id, setId] = useState('');
    const [viewStore, setViewStore] = useState({});
    const [search, setSearch] = useState('');
    const history = useHistory();
    const handleClose = () => setShowDeleteModal(false);
    const closeModal = () => {
        setShowDataModal(false)
    }


    const viewData = (dataKey) => {
        setShowDataModal(true)
        storeData.forEach(store => {
            if (store.key === dataKey) {
                setViewStore(store);
            }
        })
    }

    const editData = (dataKey) => {
        history.push(`/mailthread/${dataKey}`);
    }

    const openModal = (e) => {
        setShowDeleteModal(true)
        setId(e.target.id);

    }

    const deleteData = () => {
        storeData.forEach(store => {
            if (store.key.toString() === id) {
                storeData.splice(storeData.indexOf(store), 1);
            }
        })
        setShowDeleteModal(false)
    }

    const data = search !== '' ? storeData.filter((items) => items.name.includes(search)).map(store => (<tr key={store.key}>
        <td>{storeData.indexOf(store)}</td>
        <td>{store.name}</td>
        <td>{store.type}</td>
        <td>{store.date}</td>
        <td><Button id={store.key} variant="warning" className='me-2 my-1 my-sm-0' onClick={openModal}>DELETE</Button>
            {store.status === 1 ? <Button variant="warning" onClick={() => viewData(store.key)}>VIEW</Button> :
                <Button variant="warning" onClick={() => editData(store.key)}>EDIT</Button>}
        </td>
    </tr>)) : storeData.map(store => (<tr key={store.key}>
        <td>{storeData.indexOf(store)}</td>
        <td>{store.name}</td>
        <td>{store.type}</td>
        <td>{store.date}</td>
        <td><Button id={store.key} variant="warning" className='me-2 my-1 my-sm-0' onClick={openModal}>DELETE</Button>
            {store.status === 1 ? <Button variant="warning" onClick={() => viewData(store.key)}>VIEW</Button> :
                <Button variant="warning" onClick={() => editData(store.key)}>EDIT</Button>}
        </td>
    </tr>))

    const handleSearch = (e) => {
        setTimeout(() => {
            setSearch(e.target.value);
        }, 500);
    }

    return (
        <Container fluid className='container-style pb-3 px-0'>
            <div className='d-flex flex-row justify-content-between bg-white align-items-baseline px-3 py-3'>
                <div>
                    <h2>Campaigns</h2>
                    <p className='opacity-50'>You can communicate with your customers directly from this section.</p>
                </div>
                <div>
                    <ButtonComponent
                        name={<Link to='/mailthread' className='text-decoration-none text-white'>
                            <span>New thread</span> </Link>}/>
                </div>
            </div>
            <div className='bg-white d-flex justify-content-end '>
                <label htmlFor="search">
                    <input id="search" type="text" onChange={handleSearch} className='mx-5' placeholder="search"/>
                </label>
            </div>
            <div className='bg-white px-3'>
                <CampaingTable tdata={data}/>
            </div>

            <Modal show={showDeleteModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete</Modal.Title>
                </Modal.Header>
                <Modal.Body>Melumatlar silinsin?</Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={deleteData}>
                        Sil
                    </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showDataModal} onHide={closeModal} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Show Data</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" value={viewStore.name} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Template</Form.Label>
                            <Form.Control type="text" value={viewStore.template} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>From</Form.Label>
                            <Form.Control type="text" value={viewStore.from} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>To</Form.Label>
                            <Form.Control type="text" value={viewStore.to?.map(i => i.key)} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>If customer name is empty</Form.Label>
                            <Form.Control type="text" value={viewStore.customerName} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Start sending</Form.Label>
                            <Form.Control type="text" value={viewStore.date} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Subject</Form.Label>
                            <Form.Control type="text" value={viewStore.subject} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Content Name</Form.Label>
                            <Form.Control type="text" value={viewStore.contentName} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Feedback link</Form.Label>
                            <Form.Control type="text" value={viewStore.feedLink} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Content template</Form.Label>
                            <Form.Control type="text" value={viewStore.contentTemplate} disabled/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Content text</Form.Label>
                            <Form.Control as="textarea" row={5}
                                          value={viewStore.contentText?.replace(/<\/?[^>]+(>|$)/g, "")} disabled/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={closeModal}>
                        Bagla
                    </Button>
                </Modal.Footer>
            </Modal>
        </Container>
    )
}
export default Campaing