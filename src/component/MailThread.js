import React, {useEffect, useState} from "react";
import {Button, Container, Form, FormControl} from "react-bootstrap";
import {Row, Col} from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';
import SendingInfo from "./SendingInfo";
import ButtonComponent from "./ButtonComponent";
import '../../src/assets/scss/mailThreadCss.scss';
import {CKEditor} from 'ckeditor4-react';
import {Multiselect} from "multiselect-react-dropdown";
import {useSelector, useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import {useParams} from "react-router-dom";


const MailThread = () => {

    const params = useParams();
    const storeData = useSelector(state => state);
    const [show, setShow] = useState(false);
    const [smsCount, setSmsCount] = useState(0);
    const countMail = 1000 - parseInt(storeData && storeData[0] ? storeData[0].number : 0);

    function getval() {
        return params.id ? storeData.find(data => data.key.toString() === params.id.toString()) : {};
    }

    const [allData, setAllData] = useState(getval());


    const dispatch = useDispatch();

    const history = useHistory();
    const [reciever, setReciever] = useState([
        {key: "Option1@gmail.com", cat: "Customer"},
        {key: "Option 2", cat: "Customer"},
        {key: "Option 3", cat: "Customer"},
        {key: "Option 4", cat: "Reciever"},
        {key: "Option 5", cat: "Reciever"},
        {key: "Option 6", cat: "Reciever"},
        {key: "Option 7", cat: "Reciever"}
    ]);

    const opt = [{key: "all", value: "all"}, ...reciever];

    useEffect(() => {
        return () => {

            if (allData?.status === 0) {
                dispatch({type: 'send', newData: allData});
            }
        }
    }, [allData, dispatch])

    const handleKeypress = (e) => {
        if (e.key === 'Enter') {
            const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            if (regex.test(e.target.value.toString()) && reciever.map(data => (data.key !== e.target.value.toString()))) {
                setReciever(reciever => [{
                    key: e.target.value.toString(),
                    cat: 'Reciever',
                    value: e.target.value.toString()
                }, ...reciever])
            } else {

            }
        }
    }

    const getName = (e) => {
        e.preventDefault();
        allData.name = e.target.value.toString();
        setAllData(allData);
    }
    const getTeplate = (e) => {
        e.preventDefault();
        allData.template = e.target.value.toString()
        setAllData(allData);
    }
    const getFrom = (e) => {
        e.preventDefault();
        allData.from = e.target.value.toString();
        setAllData(allData);
    }


    const getToValue = (selectedList, selectedItem) => {
        if (selectedItem.value === "all") {
            selectedList.splice(0, selectedList.length);
            selectedList.push(...opt.slice(1));
            allData.to = selectedList;
        } else {
            allData.to = selectedList;
        }
        setAllData(allData);
    }

    const getCustomerName = (e) => {
        e.preventDefault();
        allData.customerName = e.target.value.toString();
        setAllData(allData);
    }
    const getDate = (e) => {
        allData.date = e.target.value.toString();
        setAllData(allData);
    }
    const getSubject = (e) => {
        allData.subject = e.target.value.toString();
        setAllData(allData);
    }
    const getContentName = (e) => {
        allData.contentName = e.target.value.toString();
        setAllData(allData);
    }

    const getFeedbackLink = (e) => {
        allData.feedLink = e.target.value.toString();
        setAllData(allData);
    }
    const getContentTemplate = (e) => {
        allData.contentTemplate = e.target.value.toString();
        setAllData(allData);
    }
    const getContentText = (e) => {
        allData.contentText = e.editor.getData();
        setAllData(allData);
        setSmsCount(Math.floor(e.editor.getData().replace(/<\/?[^>]+(>|$)/g, "").length / 150))
    }

    const SendClick = () => {
        setShow(true);
        allData.type = 'mail';
        allData.number = parseInt(storeData && storeData[0] ? storeData[0].number + 1 : 1);
        allData.key = params.id ? params.id : Math.random();
        setAllData(allData);
    }
    const handleCloseAccept = () => {
        setShow(false)
        allData.status = 1;
        setAllData(allData);
        dispatch({type: 'send', newData: allData});
        history.push('/');

    };
    const handleCloseInject = () => {
        setShow(false);
        history.push('/');
        dispatch({type: 'send', newData: allData});

    }

    return (
        <Container fluid className='container-style py-3'>
            <Row>
                <Col xxl={10} lg={9} md={8}>
                    <div className='bg-white px-3'>
                        <h2>Email thread</h2>
                        <hr/>
                        <Form>
                            <Row className="mb-3">
                                <Form.Group as={Col} sm="6" controlId="formGridName" className='my-sm-0 my-1'>
                                    <Form.Label>Thread Name<sub className='sub-style px-1'>*</sub></Form.Label>
                                    <Form.Control type="text" placeholder="Enter thread name" onChange={getName}
                                                  defaultValue={allData?.name}/>
                                </Form.Group>

                                <Form.Group as={Col} sm="6" controlId="formGridTemplate" className='my-sm-0 my-1'>
                                    <Form.Label>Template</Form.Label>
                                    <Form.Select onChange={getTeplate}
                                                 defaultValue={allData?.template}>
                                        <option value=''>Select feedback templeate</option>
                                        <option value="temp1">temp1</option>
                                        <option value='temp2'> temp2</option>
                                    </Form.Select>
                                </Form.Group>
                            </Row>
                            <Row className="mb-3">
                                <Form.Group as={Col} sm="6" controlId="formGridFrom" className='my-sm-0 my-1'>
                                    <Form.Label>From</Form.Label>
                                    <Form.Control type="text" placeholder="Qmeter or 2354" onChange={getFrom}
                                                  defaultValue={allData?.from}/>
                                </Form.Group>

                                <Form.Group as={Col} sm="6" controlId="formGridTo" className='my-sm-0 my-1'>
                                    <Form.Label>To<sub className='sub-style px-1'>*</sub></Form.Label>
                                    <Multiselect
                                        options={opt}
                                        groupBy="cat"
                                        displayValue="key"
                                        showCheckbox={true}
                                        placeholder="Add mail"
                                        onKeyPressFn={handleKeypress}
                                        onSelect={getToValue}
                                        selectedValues={allData?.to}
                                    />
                                </Form.Group>
                            </Row>
                            <Row className="mb-3">
                                <Form.Group as={Col} sm="6" controlId="formGridCustomerName" className='my-sm-0 my-1'>
                                    <Form.Label>If Customer name is empty</Form.Label>
                                    <Form.Control type="text" placeholder="Customer" onChange={getCustomerName}
                                                  defaultValue={allData?.customerName}/>
                                </Form.Group>

                                <Form.Group as={Col} sm="6" controlId="formGridTo" className='my-sm-0 my-1'>
                                    <Form.Label>Start sending<sub className='sub-style px-1'>*</sub></Form.Label>
                                    <Form.Control type="date" name="dob" placeholder='aaaa' onChange={getDate}
                                                  defaultValue={allData?.date}/>
                                </Form.Group>
                            </Row>
                            <Row className="mb-3">
                                <Form.Group as={Col} sm="6" controlId="formGridSubject" className='my-sm-0 my-1'>
                                    <Form.Label>Subject</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Subject here" onChange={getSubject}
                                                  defaultValue={allData?.subject}/>
                                </Form.Group>
                            </Row>
                            <hr/>
                            <h2>Content</h2>
                            <Row className="mb-3">
                                <Form.Group as={Col} xxl="2" xl="3" lg="4" controlId="formGridFeedback"
                                            className='my-2'>
                                    <FormControl type="text" placeholder="Insert name"
                                                 className='insert-name-input' onChange={getContentName}
                                                 defaultValue={allData?.contentName}/>
                                </Form.Group>
                                <Form.Group as={Col} xxl="2" xl="3" lg="4" controlId="formGridFeedback"
                                            className='my-2'>
                                    <Form.Select onChange={getFeedbackLink}
                                                 defaultValue={allData?.feedLink}>
                                        <option>Insert Feedback links</option>
                                        <option value='feed1'>feed1</option>
                                        <option value='feed2'>feed2</option>
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group as={Col} xxl="2" xl="3" lg="4" controlId="formGridInsertTemplate"
                                            className='my-2'>
                                    <Form.Select onChange={getContentTemplate}
                                                 defaultValue={allData?.contentTemplate}>
                                        <option>Insert templeate</option>
                                        <option value='temp11'>temp11</option>
                                        <option value='temp12'>temp12</option>
                                    </Form.Select>
                                </Form.Group>
                            </Row>
                            <div className='py-2'>
                                <CKEditor
                                    initData={allData?.contentText ? allData.contentText : ""}
                                    onChange={getContentText}
                                />
                            </div>
                        </Form>
                        <hr/>
                        <div className='d-flex justify-content-between py-3'>
                            <p className='opacity-75'><sup>*</sup>Dont insert link if template not selected</p>
                            <ButtonComponent name="Send" btnClick={SendClick}/>
                        </div>
                    </div>
                </Col>
                <Col xxl={2} lg={3} md={4}>
                    <SendingInfo count={countMail} total={smsCount}/>
                </Col>
            </Row>
            <Modal show={show}>
                <Modal.Header>
                    <Modal.Title>Confirm modal</Modal.Title>
                </Modal.Header>
                <Modal.Body>Melumatlar saxlanilsin?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseInject}>
                        Draft olaraq saxla
                    </Button>
                    <Button variant="primary" onClick={handleCloseAccept}>
                        Tesdiq et
                    </Button>
                </Modal.Footer>
            </Modal>
        </Container>
    )
}
export default MailThread;