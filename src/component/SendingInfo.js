import React from "react";
import {FaRocketchat} from 'react-icons/fa';
import '../assets/scss/mailThreadCss.scss'

const SendingInfo = (props) => {
    return (
        <div className='bg-white py-1 my-md-0 my-2'>
            <div className='m-0 px-2'>
                <h5>Sending info</h5>
                <hr/>
            </div>
            <div className='d-flex flex-column justify-content-center align-items-center mb-4'>
                <FaRocketchat size={28}/>
                <p className='total-count'>{props.total}</p>
                <p>Total email count</p>
            </div>
            <div className='d-flex flex-row justify-content-between px-2'>
                <p>Customer count</p>
                <p>0</p>
            </div>
            <hr className='mt-0'/>
            <div className='d-flex flex-row justify-content-between px-2'>
                <p>Feedback balance</p>
                <p>{props.count}</p>
            </div>
            <hr className='mt-0'/>
        </div>
    )
}
export default SendingInfo;