import 'bootstrap/dist/css/bootstrap.min.css';
import MailThread from "./component/MailThread";
import Campaing from "./component/campaingComponents/Campaing"
import {Route, Switch} from 'react-router-dom'
import {Fragment} from "react";

function App() {
    return (

        <Switch>
            <Route path='/'  exact>
                <Campaing/>
            </Route>
            <Route path='/mailthread' exact>
                <MailThread/>
            </Route>
            <Route path='/mailthread/:id' >
                <MailThread/>
            </Route>
        </Switch>
        // <Fragment>
        //
        //     {/*<MailThread/>*/}
        //     <Campaing/>
        // </Fragment>

    );
}

export default App;
